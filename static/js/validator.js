$(document).ready(function() {
    $('.submit_signup').click(function(){ return signup(); });
    $('.submit_login').click(function(){ return login(); });
    $('.submit_add_product').click(function(){ return add_product(); });
    $('.submit_edit_product').click(function(){ return edit_product(); });
});


function signup() {
    name = $('input[name="signup_data[name]"]').val();
    email = $('input[name="signup_data[email]"]').val();
    phone = $('input[name="signup_data[phone]"]').val();
    password = $('input[name="signup_data[password]"]').val();
    re_password = $('input[name="signup_data[re_password]"]').val();

    var errors = [];


    if (name.trim() == '')
        errors.push('Поле ФИО должно быть заполнено');
    if (email.trim() == '')
        errors.push('Поле email должно быть заполнено');
    else
    $.ajax({
        async: false,
        url: 'ajax?action=is_free_email&email='+ email,
        success: function(free) {
            if (free == 0)
                errors.push('Пользователь с таким адресом электронной почты уже существует');
        }
    });

    if (phone.trim() == '')
        errors.push('Поле Номер телефона должно быть заполнено');
    if (password.length < 8)
        errors.push('Пароль должен быть не менее 8 символов');
    if (password != re_password)
        errors.push('Пароли не совпадают');


    if (errors.length == 0){
        return true;
    }else{
        $('input[type="submit"]').parent().append( "<p class='messages' style='color: red'>"+errors[0]+"</p>" );
        setTimeout(function(){$('.messages').remove()},3000);
        return false;
    }
}

function login() {
    email = $('input[name="login_data[email]"]').val();
    password = $('input[name="login_data[password]"]').val();

    var errors = [];


    if (email.trim() == '')
        errors.push('Поле email должно быть заполнено');
    else
    $.ajax({
        async: false,
        url: 'ajax?action=is_valid_password&email='+email+'&password='+ password,
        success: function(valid) {
            if (valid == 0)
                errors.push('Неверный логин или пароль');
        }
    });


    if (errors.length == 0){
        return true;
    }else{
        $('input[type="submit"]').parent().append( "<p class='messages' style='color: red'>"+errors[0]+"</p>" );
        setTimeout(function(){$('.messages').remove()},3000);
        return false;
    }
}

function add_product() {
    console.log('+++');
    name = $('input[name="add_product_data[name]"]').val();
    price = $('input[name="add_product_data[price]"]').val();
    quantity = $('input[name="add_product_data[quantity]"]').val();
    description = $('input[name="add_product_data[description]"]').val();

    var errors = [];


    if (name.trim() == '')
        errors.push('Поле Название должно быть заполнено');
    if (price.trim() == '')
        errors.push('Поле Цена должно быть заполнено');
    if (quantity.trim() == '')
        errors.push('Поле Колличество должно быть заполнено');
    if (description.trim() == '')
        errors.push('Поле Описание должно быть заполнено');
    if (description.length > 65536)
        errors.push('Описание должно быть до 65Кб');
    if (!isNumber(price))
        errors.push('Цена должна быть числом');
    if (!(isNumber(quantity)) || !(0 <= parseFloat(quantity) && parseFloat(quantity) <= 100))
        errors.push('Колличество должно быть числом от 0 до 100');
    if (photo('product_photo')['type'] == 'undefined'){
            errors.push('Нет фото');
    }else{
        if (photo('product_photo')['type'] == 'wrong'){
            errors.push('Разрешение фото может быть только .jpg, .png, .gif');
        }
        if (photo('product_photo')['size'] > 2097151){
            errors.push('Размер фото не должен превышать 2Мб');
        }
    }



    if (errors.length == 0){
        return true;
    }else{
        $('input[type="submit"]').parent().append( "<p class='messages' style='color: red'>"+errors[0]+"</p>" );
        setTimeout(function(){$('.messages').remove()},3000);
        return false;
    }
}

function edit_product() {
    name = $('input[name="edit_product_data[name]"]').val();
    price = $('input[name="edit_product_data[price]"]').val();
    quantity = $('input[name="edit_product_data[quantity]"]').val();
    description = $('input[name="edit_product_data[description]"]').val();

    var errors = [];


    if (name.trim() == '')
        errors.push('Поле Название должно быть заполнено');
    if (price.trim() == '')
        errors.push('Поле Цена должно быть заполнено');
    if (quantity.trim() == '')
        errors.push('Поле Колличество должно быть заполнено');
    if (description.trim() == '')
        errors.push('Поле Описание должно быть заполнено');
    if (description.length > 65535)
        errors.push('Описание должно быть до 65Кб');
    if (!isNumber(price))
        errors.push('Цена должна быть числом');
    if (!(isNumber(quantity)) || !(0 <= parseFloat(quantity) && parseFloat(quantity) <= 100))
        errors.push('Колличество должно быть числом от 0 до 100');
    if (photo('product_photo')['type'] != 'undefined'){
        if (photo('product_photo')['type'] == 'wrong'){
            errors.push('Разрешение фото может быть только .jpg, .png, .gif');
        }
        if (photo('product_photo')['size'] > 2097151){
            errors.push('Размер фото не должен превышать 2Мб');
        }
    }



    if (errors.length == 0){
        return true;
    }else{
        $('input[type="submit"]').parent().append( "<p class='messages' style='color: red'>"+errors[0]+"</p>" );
        setTimeout(function(){$('.messages').remove()},3000);
        return false;
    }
}

function photo(name) {
    var photo = document.getElementsByName(name)[0].files[0];
    if (typeof photo == 'undefined')
        return {'type' : 'undefined', 'size' : 0, };
    var type = photo.type;
    var size = photo.size;
    if (type != "image/jpeg" && type != "image/png" && type != "image/gif")
        type = 'wrong';
    return {'type' : type, 'size' : size, };
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}