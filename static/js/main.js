$(document).ready(function() {
    setTimeout(function(){$('.messages').remove()},3000);
});

function delete_product(id_product) {
    if (confirm('Удалить товар #' + id_product + '?')){
        $.ajax({
            url: 'ajax?action=delete_product&id='+ id_product,
            success: function(data) {
                location.reload(true);
            }
        });
    }
}

function buy_product(id_product, name, price) {
    if (confirm('Купить ' + name + ' за ' +price+ ' BUN?')){
        $.ajax({
            url: 'ajax?action=buy_product&id='+ id_product,
            success: function(data) {
                location.reload(true);
            }
        });
    }
}
