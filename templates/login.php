<!DOCTYPE html>
<html>
<head>
    <title>Авторизация</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="../static/style/styles.css">

    <!-- js -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="../static/js/main.js"></script>
    <script type="text/javascript" src="../static/js/validator.js"></script>
</head>
<body>
    <div class="text-center"><?php include(TEMPLATE_PATH.'/components/menu.php')?></div>
    <div class="text-center">
        <div class="block-form">
            <form action="" method="post">
                <table>
                    <tr>
                        <td>
                            email:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="email" name="login_data[email]" value="test@test.test">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Пароль:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="password" name="login_data[password]" value="test">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" value="Войти" class="submit_login">
                        </td>
                    </tr>
                </table>    
            </form>
        </div>
    </div>
</body>
</html>
