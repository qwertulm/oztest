<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />

    <title>Мои товары</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="../static/style/styles.css">

    <!-- js -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="../static/js/main.js"></script>
</head>
<body>
    <div class="text-center"><?php include(TEMPLATE_PATH.'/components/menu.php')?></div>
    <div class="text-center" style="margin-top: 10px;">
        <a href="/add_product">✚ Добавить товар</a>
    </div>
    <div class="text-center">
        <div class="text-left" style="display: inline-block; margin-top: 10px;">
            <?php if (isset($products) && !empty($products)){?>
                <table class="text-center product-table">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Изображение</th>
                        <th>Название</th>
                        <th>Цена</th>
                        <th>Колличество</th>
                        <th>Колличество проданных</th>
                        <th>Дата последней покупки</th>
                        <th>Действия</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products as $product){?>
                        <tr>
                            <td>
                                <a href="product_page?id_product=<?php echo $product->id; ?>">
                                    #<?php echo $product->id; ?>
                                </a>
                            </td>
                            <td>
                                <a href="product_page?id_product=<?php echo $product->id; ?>">
                                    <img width="50" src="/products_img/<?php echo $product->id; ?>.jpg">
                                </a>
                            </td>
                            <td><?php echo $product->name; ?></td>
                            <td><?php echo $product->price; ?></td>
                            <td><?php echo $product->quantity; ?></td>
                            <td><?php echo $product->quantity_sold; ?></td>
                            <td><?php echo ($product->date_last_purchase != '0000-00-00 00:00:00')?$product->date_last_purchase:'---'; ?></td>
                            <td>
                                <a title="редактировать" style="display: inline; font-size: 20px; text-decoration:none; color: #adbf31;"
                                    href="/edit_product?id_product=<?php echo $product->id; ?>">&#x270e;</a>
                                <a title="удалить" style="display: inline; font-size: 20px; text-decoration:none; color: #ff4100;"
                                    href="javascript:delete_product(<?php echo $product->id; ?>)">&#x2718;</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php }else{?>
                Нет товаров
            <?php }?>
        </div>
    </div>
</body>
</html>
