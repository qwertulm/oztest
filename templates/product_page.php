<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />

    <title><?php echo $product->name; ?></title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="../static/style/styles.css">

    <!-- js -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="../static/js/main.js"></script>
</head>
<body>
    <div class="text-center"><?php include(TEMPLATE_PATH.'/components/menu.php')?></div>
    <div class="text-center">
        <div class="container" style="width: 1000px; background-color: #9d9e9a; border-radius: 0px 0px 5px 5px;">
            <?php if (isset($product->id)){ ?>
            <div class="row">
                <div class="col-md-7"  style="height: 400px; background-color: #d3d5d8;">
                    <img src="/products_img/<?php echo $product->id; ?>.jpg" style="max-height: 400px; max-width: 100%">
                </div>
                <div class="col-md-5"  style="height: 400px; background-color: #d3d5d8">
                    <table class="product-table" style="width: 100%; height: calc(100% - 1px); font-size: 18px;">
                        <tr>
                            <td colspan="2"><div style="font-size: 130%"><?php echo $product->name; ?></div></td>
                        </tr>
                        <tr>
                            <td colspan="2"><div style="font-size: 100%">Данные продавца:</div></td>
                        </tr>
                        <tr>
                            <td><div>ФИО: </div></td>
                            <td><div><?php echo $product->author->name; ?></div></td>
                        </tr>
                        <tr>
                            <td><div>Телефон: </div></td>
                            <td><div><?php echo $product->author->phone; ?></div></td>
                        </tr>
                        <tr>
                            <td><div>email: </div></td>
                            <td><div><?php echo $product->author->email; ?></div></td>
                        </tr>
                        <tr>
                            <td colspan="2"><div style="font-size: 200%"><?php echo $product->price; ?> BUN</div></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <a href="javascript:buy_product(<?php echo $product->id; ?>, '<?php echo $product->name; ?>' , <?php echo $product->price; ?>);" class="btn-buy">Купить</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"  style="background-color: #bdbfc1; font-size: 15px; border-radius: 0px 0px 5px 5px;">
                    <div style="font-size: 130%;">Описание</div>
                    <div style="text-align: left;"><?php echo $product->description; ?></div>
                </div>
            </div>
            <?php }else{ ?>
                Товар не найден
            <?php } ?>
        </div>
    </div>

</body>
</html>
