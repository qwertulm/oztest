<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />

    <title>Мои заказы</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="../static/style/styles.css">

    <!-- js -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="../static/js/main.js"></script>
</head>
<body>
    <div class="text-center"><?php include(TEMPLATE_PATH.'/components/menu.php')?></div>
    <div class="text-center">
        <div class="text-left" style="display: inline-block; margin-top: 10px">
            <?php if (isset($orders) && !empty($orders)){?>
                <table class="text-center product-table">
                <thead>
                    <tr>
                        <th>id товара</th>
                        <th>Изображение</th>
                        <th>Название</th>
                        <th>Цена</th>
                        <th>Колличество купленых</th>
                        <th>Дата последней покупки</th>
                        <th>email продавца</th>
                        <th>Телефон продавца</th>
                        <th>ФИО продавца</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($orders as $order){?>
                        <tr>
                            <td>
                                <a href="product_page?id_product=<?php echo $order->product->id; ?>">
                                    #<?php echo $order->product->id; ?>
                                </a>
                            </td>
                            <td>
                                <a href="product_page?id_product=<?php echo $order->product->id; ?>">
                                    <img width="50" src="/products_img/<?php echo $order->product->id; ?>.jpg">
                                </a>
                            </td>
                            <td><?php echo $order->product->name; ?></td>
                            <td><?php echo $order->product->price; ?></td>
                            <td><?php echo $order->quantity; ?></td>
                            <td><?php echo $order->date; ?></td>
                            <td><?php echo $order->product->author->email; ?></td>
                            <td><?php echo $order->product->author->phone; ?></td>
                            <td><?php echo $order->product->author->name; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php }else{?>
                Нет товаров
            <?php }?>
        </div>
    </div>
</body>
</html>
