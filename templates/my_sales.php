<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />

    <title>Мои продажи</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="../static/style/styles.css">

    <!-- js -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="../static/js/main.js"></script>
</head>
<body>
    <div class="text-center"><?php include(TEMPLATE_PATH.'/components/menu.php')?></div>
    <div class="text-center">
        <div class="text-left" style="display: inline-block; margin-top: 10px">
            <?php if (isset($sales) && !empty($sales)){?>
                <table class="text-center product-table">
                <thead>
                    <tr>
                        <th>id товара</th>
                        <th>Изображение</th>
                        <th>Название</th>
                        <th>Цена</th>
                        <th>Колличество купленых</th>
                        <th>Дата продажи</th>
                        <th>email покупателя</th>
                        <th>Телефон покупателя</th>
                        <th>ФИО покупателя</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($sales as $sale){?>
                        <tr>
                            <td>
                                <a href="product_page?id_product=<?php echo $sale->product->id; ?>">
                                    #<?php echo $sale->product->id; ?>
                                </a>
                            </td>
                            <td>
                                <a href="product_page?id_product=<?php echo $sale->product->id; ?>">
                                    <img width="50" src="/products_img/<?php echo $sale->product->id; ?>.jpg">
                                </a>
                            </td>
                            <td><?php echo $sale->product->name; ?></td>
                            <td><?php echo $sale->product->price; ?></td>
                            <td><?php echo $sale->quantity; ?></td>
                            <td><?php echo $sale->date; ?></td>
                            <td><?php echo $sale->user->email; ?></td>
                            <td><?php echo $sale->user->phone; ?></td>
                            <td><?php echo $sale->user->name; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php }else{?>
                Нет товаров
            <?php }?>
        </div>
    </div>
</body>
</html>
