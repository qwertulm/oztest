<?php include(TEMPLATE_PATH.'/components/messages.php')?>
<div class="main-menu">
    <div class="menu-item">
        <a href="/catalog">
            Каталог
        </a>
    </div>
    <?php if (User::isLogged()){?>
        <div class="menu-item">
            <a href="/my_products">
                Мои товары
            </a>
        </div>
        <div class="menu-item">
            <a href="/my_orders">
                Мой заказы
            </a>
        </div>
        <div class="menu-item">
            <a href="/my_sales">
                Мои продажи
            </a>
        </div>
        <div class="menu-item">
            <a href="/logout">
                Выйти
            </a>
        </div>
    <?php }else{ ?>
        <div class="menu-item">
            <a href="/signup">
                Регистрация
            </a>
        </div>
        <div class="menu-item">
            <a href="/login">
                Войти
            </a>
        </div>
    <?php } ?>
</div>