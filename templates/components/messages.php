<?php if (isset($_SESSION['messages'])){?>
    <div style="position: fixed; top 10px; right: 10px; z-index: 100; font-size: 130%;" class="messages">
        <?php foreach ($_SESSION['messages'] as $message){ ?>
            <div style="margin: 5px; padding: 5px; border-radius: 5px;
                        color: #fff; background-color: <?php echo $message['color']?>">
                <?php echo $message['text']?>
            </div>
        <?php } ?>
    </div>
<?php unset($_SESSION['messages']);} ?>

<?php

