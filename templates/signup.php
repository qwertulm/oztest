<!DOCTYPE html>
<html>
<head>
    <title>Регистрация</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="../static/style/styles.css">

    <!-- js -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="../static/js/main.js"></script>
    <script type="text/javascript" src="../static/js/validator.js"></script>
</head>
<body>
    <div class="text-center"><?php include(TEMPLATE_PATH.'/components/menu.php')?></div>
    <div class="text-center">
        <div class="block-form">
            <form name="signup_form" action="" method="post">
                <table>
                    <tr>
                        <td>
                            ФИО:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="signup_data[name]">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            email:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="email" name="signup_data[email]">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Номер телефона:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="signup_data[phone]">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Пароль:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="password" name="signup_data[password]">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Повтор пароля:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="password" name="signup_data[re_password]">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" class="submit_signup" value="Регистрация">
                        </td>
                    </tr>
                </table>    
            </form>
        </div>
    </div>
</body>
</html>
