<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />

    <title>Каталог</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="../static/style/styles.css">

    <!-- js -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="../static/js/main.js"></script>
</head>
<body>
    <div class="text-center"><?php include(TEMPLATE_PATH.'/components/menu.php')?></div>
    <div class="text-center">
        <div class="text-center" style="display: inline-block;">
            <div class="catalog text-center">
                <div class="menu-sort text-left">
                    <div class="menu-sort-item">Сортировать по:</div>
                    <a class="menu-sort-item <?php echo ($order=='name ASC')?'sort-asc':(($order=='name DESC')?'sort-desc':'');?>"         href="/catalog?order=<?php echo ($order=='name ASC')?'name DESC':'name ASC'?>">Названию</a>
                    <a class="menu-sort-item <?php echo ($order=='price ASC')?'sort-asc':(($order=='price DESC')?'sort-desc':'');?>"       href="/catalog?order=<?php echo ($order=='price ASC')?'price DESC':'price ASC'?>">Цене</a>
                    <a class="menu-sort-item <?php echo ($order=='date_add ASC')?'sort-asc':(($order=='date_add DESC')?'sort-desc':'');?>" href="/catalog?order=<?php echo ($order=='date_add DESC')?'date_add ASC':'date_add DESC'?>">Дате</a>
                </div>
                <?php if (isset($products) && !empty($products)){?>
                    <?php foreach ($products as $product){?>
                        <div class="catalog-item">
                            <a href="product_page?id_product=<?php echo $product->id; ?>" style="height: 100%;">
                                <img  src="/products_img/<?php echo $product->id; ?>.jpg">
                            </a>
                            <a href="product_page?id_product=<?php echo $product->id; ?>">
                                <div class="product-top"><?php echo $product->name; ?></div>
                            </a>
                            <div class="product-bottom">
                                <a href="javascript:buy_product(<?php echo $product->id; ?> , '<?php echo $product->name; ?>' , <?php echo $product->price; ?>);" class="btn-buy">Купить</a>
                                <div class="product-price"><?php echo $product->price; ?> BUN</div>
                            </div>
                        </div>
                    <?php } ?>
                <?php }else{ ?>
                    Нет товаров
                <?php }?>
            </div>
        </div>
    </div>
</body>
</html>
