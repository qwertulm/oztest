<!DOCTYPE html>
<html>
<head>
    <title>Добавить товар</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="../static/style/styles.css">

    <!-- Custom js -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="../static/js/main.js"></script>
    <script type="text/javascript" src="../static/js/validator.js"></script>
</head>
<body>
    <div class="text-center"><?php include(TEMPLATE_PATH.'/components/menu.php')?></div>
    <div class="text-center">
        <div class="block-form">
            <form enctype="multipart/form-data" action="" method="post">
                <table>
                    <tr>
                        <td>
                            Название:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="add_product_data[name]">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Цена(BUN):
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="add_product_data[price]">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Колличество:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="add_product_data[quantity]">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Описание:
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="add_product_data[description]">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="uploadbtn" class="uploadButton">Выбрать фото</label>
                            <input type="hidden" name="MAX_FILE_SIZE" value="2097151"/>
                            <input type="file" name="product_photo" id="uploadbtn" style="opacity: 0; z-index: -1; display: none">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" class="submit_add_product" value="Создать">
                        </td>
                    </tr>
                </table>    
            </form>
        </div>
    </div>
</body>
</html>
