<?php
header("Content-type: text/html; charset=utf-8");
header("Cache-Control: no-cache, must-revalidate");


/* Объявление констант */
define('ROOT_PATH', __DIR__);
define('TEMPLATE_PATH', ROOT_PATH.'/templates');
define('PRODUCTS_IMG_PATH', ROOT_PATH.'/products_img');


/* Подключение классов */
include(ROOT_PATH.'/classes/DataBase.php');
include(ROOT_PATH.'/classes/User.php');
include(ROOT_PATH.'/classes/Product.php');
include(ROOT_PATH.'/classes/Order.php');


/* Обработка данных пользователя */
session_start();
global $user;
if (isset($_SESSION['user_email'])){
    $user = User::load($_SESSION['user_email']);
}else{
    $user = new User();
}


/* Обработка запроса */
$url = $_SERVER['REQUEST_URI'];
print_r($_SERVER['REQUEST_URI']);
preg_match('/^([^?]+)(\?.*?)?(#.*)?$/', $url, $matches);
$request = (isset($matches[1])) ? $matches[1] : '';
$request = str_replace('/', '', $request);

if ($request == '')
    $request = 'catalog';

$only_logged = ['my_products', 'my_orders', 'my_sales', ];
if (!User::isLogged() && in_array($request, $only_logged)){
    $request = '404';
}


/* Подключение контроллера */
$controller_path = 'controllers/'.$request.'.php';
if (file_exists($controller_path)){
    include($controller_path);
}else{
    include('controllers/404.php');
}




