<?php

class User{
    public $name = '';
    public $email = '';
    public $phone = '';
    public $password = '';

	function __construct($data=[]){
        if (is_array($data)){
            foreach ($data as $key=>$value){
                $this->$key = $value;
            }
        }

    }

    /* Создание нового пользователя */
    static function register($data=[]){
		$user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->password = md5($data['password']);
        $user->save();
        User::login($data['email'], $data['password']);
	}

    /* Сохранение нового пользователя */
	public function save(){
		$db = DataBase::getDB();
        $query = "INSERT INTO `users` (`email`, `name`, `phone`, `password`) 
                    VALUES ('{$this->email}', '{$this->name}', '{$this->phone}', '{$this->password}');";
        return $db->query($query);
	}

    /* Авторизация пользователя */
	static function login($email, $password){
        $user = User::load($email);
        if ($user->email == $email && $user->password == md5($password)){
            $_SESSION['user_email'] = $user->email;
            header('Location: \catalog');
            return true;
        }
        else{
            return false;
        }
    }

    /* Выход пользователя */
    public function logout(){
        unset($_SESSION['user_email']);
    }

    /* Заполнение данных пользователя из базы по email */
	static function load($email){
        $db = DataBase::getDB();
        $query = "SELECT * FROM `users` WHERE email = '{$email}'";
        $result = $db->selectRow($query);
        return new User($result);
    }

    /* Заполнение данных пользователя из базы по id */
	static function loadById($id){
        $db = DataBase::getDB();
        $query = "SELECT * FROM `users` WHERE id = '{$id}'";
        $result = $db->selectRow($query);
        return new User($result);
    }

    /* Авторизован ли пользователь */
	static function isLogged(){
        return (isset($_SESSION['user_email']) && $_SESSION['user_email'] != '');
    }

    /* Свободен ли email для регистрации */
	static function isFreeEmail($email){
        $db = DataBase::getDB();
        $query = "SELECT * FROM `users` WHERE email = '{$email}'";
        $result = $db->selectRow($query);
        if (empty($result))
            return true;
        else
            return false;
    }

    /* Существует ли пользователь с таким email и паролем */
	static function isValidPassword($email, $password){
        $db = DataBase::getDB();
        $query = "SELECT * FROM `users` WHERE email = '{$email}' AND password = '".md5($password)."' ";
        $result = $db->selectRow($query);
        if (empty(!$result))
            return true;
        else
            return false;
    }
}