<?php

class Order{
    public $id = 0;
    public $id_product = 0;
    public $product = '';
    public $id_user = 0;
    public $user = '';
    public $date = '';
    public $quantity = 1;

    /* Создание нового заказа */
	static function save($id_product){
		$db = DataBase::getDB();
		$product = Product::load($id_product);
        if (!$product->buy())
            return false;

		global $user;
        $query = "INSERT INTO `orders` (`id_product`, `id_user`, `date`) VALUES ('{$id_product}', '{$user->id}', NOW());";

        return $db->query($query);
	}

	/* Получение всех товаров, которые купил пользователь */
	static function getUserOrders($id_user){
        $db = DataBase::getDB();
        $query = "SELECT * FROM `orders` WHERE id_user = $id_user ORDER BY date DESC";
        $result = $db->select($query);
        $orders = [];
        if ($result){
            foreach ($result as $item){
                $id_product = $item['id_product'];
                if (isset($orders[$id_product])){
                    $orders[$id_product]->quantity++;
                }else{
                    $order = new Order();
                    $order->id = $item['id'];
                    $order->id_product = $item['id_product'];
                    $order->product = Product::load($item['id_product']);
                    $order->date = $item['date'];
                    $order->quantity = 1;
                    $orders[$id_product] = $order;
                }
            }
        }
        return $orders;
    }

    /* Получение всех заказов товаров, которых создавал пользователь */
	static function getUserSales($id_user){
        $db = DataBase::getDB();
        $query = "SELECT * FROM `orders` 
                  WHERE id_product IN (SELECT id FROM products WHERE id_author = {$id_user}) 
                  ORDER BY date DESC";
        $result = $db->select($query);
        $sales = [];
        if ($result){
            foreach ($result as $item){
                $sale = new Order();
                $sale->id = $item['id'];
                $sale->id_product = $item['id_product'];
                $sale->product = Product::load($item['id_product']);
                $sale->id_user = $item['id_product'];
                $sale->user = User::loadById($item['id_user']);
                $sale->date = $item['date'];
                $sale->quantity = 1;
                $sales[$sale->id] = $sale;
            }
        }
        return $sales;
    }
}