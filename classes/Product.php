<?php

class Product{
    public $id = 0;
    public $id_author = 0;
    public $author = '';
    public $name = '';
    public $price = 0;
    public $quantity = 0;
    public $quantity_sold = 0;
    public $description = '';
    public $date_add = '';
    public $date_update = '';
    public $date_last_purchase = '';

	function __construct($data = []){
	    foreach ($data as $key=>$value){
            $this->$key = $value;
        }
        if (isset($this->id_author)){
            $this->author = User::loadById($this->id_author);
        }
    }

    /* Создание нового товара */
	static function addNew($data = [], $photo = false){
	    $product = new Product();
        $product->name = $data['name'];
        $product->price = $data['price'];
        $product->quantity = $data['quantity'];
        $product->description = $data['description'];
        $product->quantity_sold = 0;
        $id_product = $product->save();
        if (!$id_product)
            return false;
        $product->update_img($photo, $id_product);
        return true;
	}

    /* Сохранение нового товара */
	public function save(){
		$db = DataBase::getDB();
		global $user;
        $query = "INSERT INTO `products` (`id_author`, `name`, `price`, `description`, `date_add`, `date_update`, `quantity`, `quantity_sold`)
                    VALUES ('{$user->id}', '{$this->name}', '{$this->price}', '{$this->description}', NOW(), NOW(), '{$this->quantity}', '{$this->quantity_sold}');";

        return $db->query($query);
	}

    /* Редактирование товара */
    static function edit($data = [], $photo = false){
        $product = Product::load($data['id']);
        foreach ($data as $key=>$value){
            $product->$key = $value;
        }
        $product->update();
        $product->update_img($photo, $product->id);
    }

    /* Обновление товара */
	public function update(){
		$db = DataBase::getDB();
        $query = "UPDATE `products` 
            SET `id_author` = '{$this->id_author}',
                `name` = '{$this->name}',
                `price` = '{$this->price}',
                `description` = '{$this->description}',
                `quantity` = '{$this->quantity}',
                `date_update` = NOW()
            WHERE id = $this->id
                ";
        $db->query($query);
	}

    /* Заполнение данных товара из базы*/
	static function load($id_product){
        $db = DataBase::getDB();
        $query = "SELECT * FROM `products` WHERE id = '{$id_product}'";
        $result = $db->selectRow($query);
        if (empty($result))
            return false;
        return new Product($result);
    }

    /* Покупка нового товара (изменение колличества и обновление даты последней покупки) */
    public function buy(){
        $db = DataBase::getDB();
        if ($this->quantity <= 0)
            return false;

        $this->quantity--;
        $this->quantity_sold++;
        $query = "UPDATE `products` 
            SET `quantity` = '{$this->quantity}',
                `quantity_sold` = '{$this->quantity_sold}',
                `date_last_purchase` = NOW()
            WHERE id = $this->id
                ";
        $db->query($query);
        return true;
    }

    /* Удаление товара и его картинку */
    static function delete($id){
        $db = DataBase::getDB();
        $query = "DELETE FROM `products` WHERE id = {$id}";
        $result = $db->query($query);
        if (!$result){
            return false;
        }
        unlink(PRODUCTS_IMG_PATH.'/'.$id.'.jpg');
        return true;
    }

    /* Возвращает товары, созданные пользователем */
	static function getProductsByUserId($id_user){
        $db = DataBase::getDB();
        $query = "SELECT * FROM `products` WHERE id_author = '{$id_user}'";
        $result = $db->select($query);
        $products = [];
        if ($result){
            foreach ($result as $item){
                $products[] = new Product($item);
            }
        }
        return $products;
    }

    /* Возвращает все товары для каталога и сортирует */
	static function getProductsForCatalog($order){
        $db = DataBase::getDB();
        $query = "SELECT * FROM `products` WHERE quantity > 0 ORDER BY {$order}";
        $result = $db->select($query);
        $products = [];
        if ($result){
            foreach ($result as $item){
                $products[] = new Product($item);
            }
        }
        return $products;
    }

    /* Обновляет изображение товара */
    public function update_img($photo = false, $id_product = false){
        if ($photo && $id_product){
            $tmp = explode('.', $photo['name']);
            $ext = end($tmp);
            $new_name = $id_product.'.jpg';
            $full_path = PRODUCTS_IMG_PATH .'/'.$new_name;
            move_uploaded_file($photo['tmp_name'], $full_path);
        }
    }

}