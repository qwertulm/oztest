<?php
    /* Редактирование товара, если был запрос */
    if (isset($_POST['edit_product_data'])){
        $photo = false;
        if (isset($_FILES['product_photo'])){
            $photo = $_FILES['product_photo'];
        }
        Product::edit($_POST['edit_product_data'], $photo);
        header('Location: \my_products');
    }

    $product = Product::load($_GET['id_product']);

    /* Подключение шаблона */
    include(TEMPLATE_PATH.'/edit_product.php');
?>
