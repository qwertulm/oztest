<?php
    if (isset($_GET['id_product'])){
        $id_product = $_GET['id_product'];
        $product = Product::load($id_product);

        /* Подключение шаблона */
        include(TEMPLATE_PATH.'/product_page.php');
    }else{
        include(TEMPLATE_PATH.'/404.php');
    }
?>
