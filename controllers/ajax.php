<?php
$action = $_GET['action'];

if ($action == 'delete_product'){
    $id_product = $_GET['id'];
    if  (Product::delete($id_product)){
        $_SESSION['messages'][] = ['text' => 'Товар удалён', 'color' => 'green', ];
    }else{
        $_SESSION['messages'][] = ['text' => 'Товар не удалён, возможно существуют заказы с этим товаром', 'color' => 'red', ];
    }
}

if ($action == 'buy_product'){
    $id_product = $_GET['id'];
    if (!$user->isLogged()){
        $_SESSION['messages'][] = ['text' => 'Товар не заказан, войдите или зарегистрируйтесь', 'color' => '#947b01', ];
        die();
    }
    $product = Product::load($id_product);
    if ($product->id_author == $user->id){
        $_SESSION['messages'][] = ['text' => 'Нельзя заказать свой товар', 'color' => 'red', ];
        die();
    }
    if ($product->quantity <= 0 ){
        $_SESSION['messages'][] = ['text' => 'Товар закончился', 'color' => 'red', ];
        die();
    }
    if  (Order::save($id_product)){
        $_SESSION['messages'][] = ['text' => 'Товар заказан', 'color' => 'green', ];
    }else{
        $_SESSION['messages'][] = ['text' => 'Товар не заказан, произошла ошибка', 'color' => 'red', ];
    }
}

if ($action == 'is_free_email'){
    $email = $_GET['email'];
    echo intval(User::isFreeEmail($email));

}

if ($action == 'is_valid_password'){
    $email = $_GET['email'];
    $password = $_GET['password'];
    echo intval(User::isValidPassword($email, $password));

}