<?php
    /* Создание нового товара, если был запрос */
    if (isset($_POST['add_product_data'])){
        $photo = false;
        if (isset($_FILES['product_photo'])){
            $photo = $_FILES['product_photo'];
        }
        if  (Product::addNew($_POST['add_product_data'], $photo)){
            $_SESSION['messages'][] = ['text' => 'Товар создан', 'color' => 'green', ];
        }else{
            $_SESSION['messages'][] = ['text' => 'Товар не создан, произошла ошибка', 'color' => 'red', ];
        }
    }

    include(TEMPLATE_PATH.'/add_product.php');
?>
